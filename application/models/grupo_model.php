<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Grupo_model extends CI_Model {

    function listaGrupos() {
        $this->db->select('GRU.id, GRU.numero, GRU.cor');
        $this->db->from('grupos as GRU');
        $this->db->order_by("numero", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    function update($grupoInfo, $numero) {
        $this->db->where('numero', $numero);       
        $this->db->update('grupos', $grupoInfo);
        return TRUE;
    }

}

  