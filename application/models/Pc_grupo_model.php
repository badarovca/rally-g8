<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Pc_grupo_model extends CI_Model {
    
    function listaPcGruposByPC($pc_ordem) {
        $this->db->select(
            'PCG.id, 
            PCG.grupo_numero, 
            PCG.pc_ordem, 
            PCG.tempo,  
            PCG.pontos, 
            PCG.passou,
            PCG.lg_atividade,
            GR.cor');
        $this->db->from('pc_grupo as PCG');
        $this->db->join('grupos as GR', 'PCG.grupo_numero = GR.numero', 'left');        
        $this->db->order_by("grupo_numero", "asc");
        $this->db->where('PCG.pc_ordem', $pc_ordem);
        $query = $this->db->get();
        return $query->result();
    }

    function listaPcGruposByGrupo($grupo_numero) {
        $this->db->select(
            'PCG.id, '
            . 'PCG.pc_ordem, '
            . 'PCG.lg_passou, '
            . 'PCG.passou, '
            . 'PCG.lg_atividade, '
            . 'PCG.tempo as tempoGrupo, '
            . 'PCS.tempo as tempoPC ');
        $this->db->from('pc_grupo as PCG');
        $this->db->join('pcs as PCS', 'PCG.pc_ordem = PCS.ordem', 'left');
        $this->db->order_by("pc_ordem", "asc");
        $this->db->where('PCG.grupo_numero', $grupo_numero);
        $query = $this->db->get();
        return $query->result();
    }    

    function listaPcGrupos() {
        $this->db->select(
            'PCG.id, 
            PCG.grupo_numero, 
            PCG.pc_ordem, 
            PCG.tempo, 
            PCG.pontos, 
            PCG.passou');
        $this->db->from('pc_grupo as PCG');
        $this->db->order_by("grupo_numero", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    function update($pcGrupoInfo, $pcGrupoId) {
        $this->db->where('id', $pcGrupoId);       
        $this->db->update('pc_grupo', $pcGrupoInfo);
        return TRUE;
    }
    
    function getPcGrupo($pcGrupoId) {
        $this->db->select(
            'PCG.id, 
            PCG.grupo_numero, 
            PCG.pc_ordem, 
            PCG.tempo, 
            PCG.lg_passou,
            PCG.lg_atividade,
            PCG.pontos, 
            PCG.passou');
        $this->db->from('pc_grupo as PCG');
        $this->db->where('PCG.id', $pcGrupoId);
        $query = $this->db->get();
        return $query->row_array();
    }

    function limpaTabela() {
        $this->db->empty_table('pc_grupo');
    }

    function newPCGrupo($userInfo) {
        $this->db->trans_start();
        $this->db->query('ALTER TABLE pc_grupo AUTO_INCREMENT 1');
        $this->db->insert('pc_grupo', $userInfo);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getPcGrupoAnterior($pc_ordem, $grupo_numero) {
        $this->db->select(
            'PCG.id, 
            PCG.grupo_numero, 
            PCG.pc_ordem, 
            PCG.tempo, 
            PCG.pontos, 
            PCG.passou');
        $this->db->from('pc_grupo as PCG');
        $this->db->where('PCG.grupo_numero', $grupo_numero);
        $this->db->Where('PCG.pc_ordem', $pc_ordem - 1);
        $query = $this->db->get();
        return $query->row_array();
    }

    function listaPcGruposGrouped() {
        $this->db->select('PCG.grupo_numero, SUM(PCG.pontos) as pontuacao, GRU.posicao');
        $this->db->from('pc_grupo as PCG');
        $this->db->join('grupos as GRU', 'PCG.grupo_numero = GRU.numero', 'left');
        $this->db->group_by("PCG.grupo_numero");
        $this->db->order_by("pontuacao", "desc");
        $query = $this->db->get();
        return $query->result();
    }

}

  