<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-users" aria-hidden="true"></i> PC <?php echo $pcOrdem ?>
      <small>Gerenciamento</small>
    </h1>
  </section>
  
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Lista de Grupos</h3>
          </div><!-- /.box-header -->

          <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
              <tr>
                <th class="text-left">Grupo</th>
                <th class="text-center">Passou às | Tempo</th>
                <th class="text-center">Atividade</th>
                <th class="text-right">AÇÕES</th>
              </tr>
              <?php
              if(!empty($listaPcGrupos)) {
                foreach($listaPcGrupos as $pcGrupo) {
              ?>
              <tr>
                <td class="text-left"> 
                    <span class="">
                        <small class="label bg-<?php echo $pcGrupo->cor;?>">&nbsp;&nbsp;</small>
                    </span>
                    &nbsp;Grupo <?php echo $pcGrupo->grupo_numero; ?>
                </td>
                <td class="text-center">
                  <?php
                  if($pcGrupo->passou) {
                    if($pcOrdem != 0) {
                      echo date("H:i:s", strtotime($pcGrupo->passou)).' | '.$pcGrupo->tempo;
                    } else {
                      echo date("H:i:s", strtotime($pcGrupo->passou));
                    }
                  } else {
                    if($pcOrdem != 0) {
                      $title = "Passou";
                    } else {
                      $title = "Iniciar";
                    }
                  ?>
                  <div class="bt-passou">
                    <a type="button" class="btn btn-sm btn-success" data-id="<?php echo $pcGrupo->id; ?>">
                      <i class="fa fa-flag-checkered"></i> <?php echo $title; ?>
                    </a>
                  </div>
                  <?php } ?>
                </td>
                <td class="text-center">
                    <?php
                    if($pcGrupo->lg_atividade == 1) {
                    ?> 
                        <i class="fa fa-fw fa-check"></i>
                    <?php
                    } else {
                    ?>                    
                    <div class="bt-executou">
                        <a type="button" class="btn btn-sm btn-primary" data-id="<?php echo $pcGrupo->id; ?>">
                            <i class="fa fa-child"></i> Executou
                        </a>
                    </div>
                    <?php 
                    } 
                    ?>
                </td>                
                <td class="text-right">
                  <?php if($pcGrupo->passou) { ?>
                  <div class="bt-correcao">
                    <a type="button" class="btn btn-sm btn-info" data-id="<?php echo $pcGrupo->id; ?>">
                      <i class="fa fa-edit"></i> Corrigir
                    </a>
                  </div>
                  <?php } ?>
                </td>
              </tr>
              <?php
                }
              }
              ?>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-2 pull-left">
        <a type="button" class="btn btn-warning btn-block" href="javascript:history.go(-1)"><span class="glyphicon glyphicon-arrow-left"></span> Voltar</a>
      </div>
    </div>

  </section>
</div>

<input id="input-ajax-base-url" type="hidden" value="<?php echo base_url('pc/'); ?>">

<script src="<?php echo base_url('assets/js/pc.js');?>"></script>