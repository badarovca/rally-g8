<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?php echo $pageTitle; ?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <!-- FontAwesome 4.3.0 -->
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url('assets/css/estilos.css') ?>">

    <style>
    	.error{
    		color:red;
    		font-weight: normal;
    	}
    </style>
    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>assets/js/jQuery-2.1.4.min.js"></script>
    <script type="text/javascript">
        var baseURL = "<?php echo base_url(); ?>";
    </script>

    <?php
      //PEGA HORA ATUAL DO SERVIDOR
      date_default_timezone_set('Etc/GMT');
      $hoje = getdate();
    ?>
    <script>
      var d = new Date(Date.UTC(<?php echo $hoje['year'].",".$hoje['mon'].",".$hoje['mday'].",".$hoje['hours'].",".$hoje['minutes'].",".$hoje['seconds']; ?>));      
      setDate();
      function setDate() {
        $.ajax({
          url: '<?php echo base_url('pc/timer'); ?>',
          dataType: 'json',
          method: 'GET',
          success: function (data) {
            d = new Date(Date.UTC(data['year'], data['mon'], data['mday'], data['hours'], data['minutes'], data['seconds']));
          },
          error: function(jqxhr, status, exception) {
            alert(exception);
          }
        });
      }
      setInterval(
        function() {
          setDate();
        }, 10000
      );
      setInterval(
        function() {
          d.setSeconds(d.getSeconds() + 1);
          //EXIBE O HORÁRIO COM 2 DIGITOS
          document.getElementById("timer").innerHTML = ("0" + d.getHours()).slice(-2) +':' + ("0" + d.getMinutes()).slice(-2) + ':' +  ("0" + d.getSeconds()).slice(-2);
        }, 1000
      );
    </script>
    
    <script src="<?php echo base_url('assets/js/sweetalert2.all.min.js');?>"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <!-- <body class="sidebar-mini skin-black-light"> -->
  <body class="skin-yellow sidebar-mini">

    <p hidden id="hours"><?php echo $hours; ?></p>
    <p hidden id="minutes"><?php echo $minutes; ?></p>
    <p hidden id="seconds"><?php echo $seconds; ?></p>

    <div class="wrapper">
      <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo base_url(); ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>R</b>G8</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b><span id="timer"></span></b> | <b>Rally</b>G8</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <?php if($role == ROLE_ADMIN) { ?>
                    <img src="<?php echo base_url(); ?>assets/dist/img/adm.png" class="user-image" alt="User Image"/>
                  <?php } elseif($role == ROLE_MANAGER) { ?>
                    <img src="<?php echo base_url(); ?>assets/dist/img/manager.jpg" class="user-image" alt="User Image"/>
                  <?php } else { ?>
                    <img src="<?php echo base_url(); ?>assets/dist/img/pc.png" class="user-image" alt="User Image"/>
                  <?php } ?>
                  <span class="hidden-xs"><?php echo $name; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <?php if($role == ROLE_ADMIN) { ?>
                      <img src="<?php echo base_url(); ?>assets/dist/img/adm.png" class="img-circle" alt="User Image" />
                    <?php } elseif($role == ROLE_MANAGER) { ?>
                      <img src="<?php echo base_url(); ?>assets/dist/img/manager.jpg" class="img-circle" alt="User Image" />
                    <?php } else { ?>
                      <img src="<?php echo base_url(); ?>assets/dist/img/pc.png" class="img-circle" alt="User Image" />
                    <?php } ?>
                    <p>
                      <?php echo $name; ?>
                      <small><?php echo $role_text; ?></small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-right">
                      <a href="<?php echo base_url(); ?>logout" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sair</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MENU PRINCIPAL</li>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>home">
                <i class="fa fa-home"></i> <span>Home</span></i>
              </a>
            </li>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>pcs">
                <i class="fa fa-flag"></i>
                <span>Pontos de Cronometragem</span>
              </a>
            </li>
            <?php if($role == ROLE_ADMIN || $role == ROLE_MANAGER) { ?>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>grupos">
                <i class="fa fa-users"></i>
                <span>Grupos</span>
              </a>
            </li>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>configuracao">
                <i class="fa fa-gear"></i>
                <span>Configuração</span>
              </a>
            </li>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>resultado">
                <i class="fa fa-flag-checkered"></i>
                <span>Resultado</span>
              </a>
            </li>
            <!--
            <li class="treeview">
              <a href="<?php echo base_url(); ?>relatorios">
                <i class="fa  fa-file-pdf-o"></i>
                <span>Relatórios</span>
              </a>
            </li>
            -->
            <?php } if($role == ROLE_ADMIN) { ?>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>userListing">
                <i class="fa fa-user"></i>
                <span>Usuários</span>
              </a>
            </li>
            <?php } ?>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>