<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-users" aria-hidden="true"></i> Configuração
      <small>Configurações do Rally</small>
    </h1>
  </section>
  
  <section class="content">

    <!--
    <a class="btn btn-app" href="#">
      <i class="fa fa-flag"></i> PCs
    </a>

    <a class="btn btn-app" href="#">
      <i class="fa fa fa-users"></i> Grupos
    </a>
    -->    

    <a class="btn btn-app" href="<?php echo base_url() ?>gerarTabelas">
      <i class="fa fa-wrench"></i> Gerar Tabelas
    </a>

  </section>
</div>