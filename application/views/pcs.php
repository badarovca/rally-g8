<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fa fa-users" aria-hidden="true"></i> Pontos de Cronometragem
            <small>Gerenciamento de PCs</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <?php
            if(!empty($listaPCs)) {
                foreach($listaPCs as $pc) {
            ?>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-green box-pc">
                <div class="inner">
                    <h3>PC <?php echo $pc->ordem ?></h3>
                    <p><?php echo $pc->nome ?></p>
                </div>
                <div class="icon"><?php echo $pc->ordem ?></div>
                <a href="<?php echo base_url().'pc/'.$pc->ordem ?>" class="small-box-footer">Abrir <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <?php
                }
            }
            ?>
        </div>    
    </section>
</div>