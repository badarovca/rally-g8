<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-flag-checkered" aria-hidden="true"></i> Resultado
      <small>Resultado do Rally G8</small>
    </h1>
  </section>
    
  <section class="content">
    
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Pontuação dos Grupos</h3>
          </div>
          <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
              <tr>
                <th>Posição</th>
                <th>Grupo</th>
                <th>Pontuação</th>
                <th class="text-center">AÇÕES</th>
              </tr>
              <?php
                if($listaPcGruposGrouped) {
                  foreach($listaPcGruposGrouped as $pcGrupoGrouped) {
              ?>
              <tr>
                <td><?php echo $pcGrupoGrouped->posicao ?></td>
                <td>Grupo <?php echo $pcGrupoGrouped->grupo_numero ?></td>
                <td><?php echo $pcGrupoGrouped->pontuacao ?></td>
                <td class="text-center">
                  <a class="btn btn-sm btn-info" href="#"><i class="fa fa-eye"></i></a>
                </td>
              </tr>
              <?php
                  }
                }
              ?>              
            </table>        
          </div>
        </div>
      </div>
    </div>

  </section>

</div>