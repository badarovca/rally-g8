<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Grupo (PCController)
 * Descrição.
 * @author : Lucas Badaró
 * @version : 0.1
 * @since : 18 Março 2018
 */
class Pc extends BaseController
{
    /**
     * Este é p construtor padrão da classe
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pc_model');
        $this->load->model('Grupo_model');
        $this->load->model('Pc_grupo_model');
        $this->isLoggedIn();   
    }
    
    /**
     * Essa função é usada para carregar a primeira tela do Grupo
     */
    public function index() {
        $data['listaPCs'] = $this->Pc_model->listaPCs();
        $this->global['pageTitle'] = 'RallyG8 : Pontos de Cronometragem';
        $this->loadViews("pcs", $this->global, $data, NULL);
    }
    
    public function gerenciaPC($pcOrdem) {
        $data['listaPcGrupos'] = $this->Pc_grupo_model->listaPcGruposByPC($pcOrdem);
        $data['pcOrdem'] = $pcOrdem;
        $this->global['pageTitle'] = 'RallyG8 : PC '.$pcOrdem;
        $this->loadViews("pc", $this->global, $data, NULL);
    }

    public function update($pcGrupoInfo, $pcGrupoId) {
        $result = $this->Pc_grupo_model->update($pcGrupoInfo, $pcGrupoId);
        $data['pcGrupo'] = $this->Pc_grupo_model->getPcGrupo($pcGrupoId);
        $data['listaGrupos'] = $this->Pc_grupo_model->listaPcGruposByPC($data['pcGrupo']['pc_ordem']);
        $data['pcOrdem'] = $data['pcGrupo']['pc_ordem'];
        $this->global['pageTitle'] = 'RallyG8 : PC '.$data['pcOrdem'];
        redirect('pc/'.$data['pcOrdem']);
    }

    public function atividade() {
        $pcGrupoId = $this->input->post("pcGrupoId");
        $pcGrupoInfo = array('lg_atividade' => 1);
        $result = $this->Pc_grupo_model->update($pcGrupoInfo, $pcGrupoId);
        $json = json_encode($result);
        echo $json;
    }

    public function passou() {
      $pcGrupoId = $this->input->post("pcGrupoId");
      date_default_timezone_set("Brazil/East");
      $now = getdate();
      $nowDateTime = date($now['hours'].":".$now['minutes'].":".$now['seconds']);
      $data['pcGrupo'] = $this->Pc_grupo_model->getPcGrupo($pcGrupoId);
      if($data['pcGrupo']['pc_ordem'] != 0) {
          $pcGrupoAnterior = $this->Pc_grupo_model->getPcGrupoAnterior($data['pcGrupo']['pc_ordem'], $data['pcGrupo']['grupo_numero']);
          if($pcGrupoAnterior['passou']) {
              $d1 = new DateTime($pcGrupoAnterior['passou']);
              $d2 = new DateTime($nowDateTime);
              $diff = $d1->diff($d2, true);
              $tempoAtual = date($diff->h.':'.$diff->i.':'.$diff->s);
          } else {
              $tempoAtual = null;    
          }
      } else {
          $tempoAtual = date("00:00:00");
      }
      $pcGrupoInfo = array('passou'=>$nowDateTime, 'tempo'=>$tempoAtual, 'lg_passou'=>1);
      $result = $this->Pc_grupo_model->update($pcGrupoInfo, $pcGrupoId);
      $json = json_encode($result);
      echo $json;
  }

  public function correcao() {
    date_default_timezone_set("Brazil/East");
    $pcGrupoId = $this->input->post("pcGrupoId");
    $horario = $this->input->post("horario");
    $data['pcGrupo'] = $this->Pc_grupo_model->getPcGrupo($pcGrupoId);
    if($data['pcGrupo']['pc_ordem'] != 0) {
      $pcGrupoAnterior = $this->Pc_grupo_model->getPcGrupoAnterior($data['pcGrupo']['pc_ordem'], $data['pcGrupo']['grupo_numero']);
      if($pcGrupoAnterior['passou']) {
          $d1 = new DateTime($pcGrupoAnterior['passou']);
          $d2 = new DateTime($horario);
          $diff = $d1->diff($d2, true);
          $tempoAtual = date($diff->h.':'.$diff->i.':'.$diff->s);
      } else {
          $tempoAtual = null;    
      }
    } else {
      $tempoAtual = date("00:00:00");
    }
    $pcGrupoInfo = array('passou'=>$horario, 'tempo'=>$tempoAtual, 'lg_passou'=>1);
    $result = $this->Pc_grupo_model->update($pcGrupoInfo, $pcGrupoId);
    $json = json_encode($result);
    echo $json;
}      

public function timer() {
  date_default_timezone_set('Etc/GMT');
  $now = getdate();
  $json = json_encode($now);
  echo $json;
}      
    /*
    public function passou() {
      $pcGrupoId = $this->input->post("pcGrupoId");
      date_default_timezone_set("Brazil/East");
      $now = getdate();
      $nowDateTime = date($now['hours'].":".$now['minutes'].":".$now['seconds']);
      $data['pcGrupo'] = $this->Pc_grupo_model->getPcGrupo($pcGrupoId);
      if($data['pcGrupo']['pc_ordem'] != 0) {
          $pcGrupoAnterior = $this->Pc_grupo_model->getPcGrupoAnterior($data['pcGrupo']['pc_ordem'], $data['pcGrupo']['grupo_numero']);
          if($pcGrupoAnterior['passou']) {
              $d1 = new DateTime($pcGrupoAnterior['passou']);
              $d2 = new DateTime($nowDateTime);
              $diff = $d1->diff($d2, true);
              $tempoAtual = date($diff->h.':'.$diff->i.':'.$diff->s);
          } else {
              $tempoAtual = null;    
          }
      } else {
          $tempoAtual = date("00:00:00");
      }
      $pcGrupoInfo = array('passou'=>$nowDateTime, 'tempo'=>$tempoAtual, 'lg_passou'=>1);
      $result = $this->Pc_grupo_model->update($pcGrupoInfo, $pcGrupoId);
      $data['pcGrupo'] = $this->Pc_grupo_model->getPcGrupo($pcGrupoId);
      //$data['listaGrupos'] = $this->Pc_grupo_model->listaPcGruposByPC($data['pcGrupo']['pc_ordem']);
      //$data['pcOrdem'] = $data['pcGrupo']['pc_ordem'];
      $resposta['nowDateTime'] = $nowDateTime;
      $resposta['tempoAtual'] = $tempoAtual;
      print_r($data);
      die();
      //$json = json_encode($data);
      //echo $json;
    }


    public function passou($pcGrupoId) {
        date_default_timezone_set("Brazil/East");
        $now = getdate();
        $nowDateTime = date($now['hours'].":".$now['minutes'].":".$now['seconds']);
        $data['pcGrupo'] = $this->Pc_grupo_model->getPcGrupo($pcGrupoId);
        if($data['pcGrupo']['pc_ordem'] != 0) {
            $pcGrupoAnterior = $this->Pc_grupo_model->getPcGrupoAnterior($data['pcGrupo']['pc_ordem'], $data['pcGrupo']['grupo_numero']);
            if($pcGrupoAnterior['passou']) {
                $d1 = new DateTime($pcGrupoAnterior['passou']);
                $d2 = new DateTime($nowDateTime);
                $diff = $d1->diff($d2, true);
                $tempoAtual = date($diff->h.':'.$diff->i.':'.$diff->s);
            } else {
                $tempoAtual = null;    
            }
        } else {
            $tempoAtual = date("00:00:00");
        }
        $pcGrupoInfo = array('passou'=>$nowDateTime, 'tempo'=>$tempoAtual, 'lg_passou'=>1);
        $this->update($pcGrupoInfo, $pcGrupoId);
    }
    */

}

?>