<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Relatorios extends BaseController {

  public function __construct() {
    parent::__construct();
    //$this->load->model('Pc_model');
    //$this->load->model('Grupo_model');
    //$this->load->model('Pc_grupo_model');
    $this->isLoggedIn();
  }

  public function index() {
    $this->global['pageTitle'] = 'RallyG8 : Relatórios';
    $this->loadViews("relatorios", $this->global, NULL, NULL);
  }

}

?>