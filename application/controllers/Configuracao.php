<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Configuracao extends BaseController {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('Pc_model');
        $this->load->model('Grupo_model');
        $this->load->model('Pc_grupo_model');
        $this->isLoggedIn();
    }
    
    public function index() {
        $this->global['pageTitle'] = 'RallyG8 : Configuração';
        $this->loadViews("configuracao", $this->global, NULL, NULL);
    }

    public function gerarTabelas() {
        $data['listaPCs'] = $this->Pc_model->listaPCs();
        $data['listaGrupos'] = $this->Grupo_model->listaGrupos();
        $this->Pc_grupo_model->limpaTabela();
        foreach($data['listaPCs'] as $pc) {
            foreach($data['listaGrupos'] as $grupo) {
                $grupo_numero = $grupo->numero;
                $pc_ordem = $pc->ordem;
                $userInfo = array('grupo_numero'=>$grupo_numero, 'pc_ordem'=>$pc_ordem);
                $this->Pc_grupo_model->newPCGrupo($userInfo);                
            }
        }
        redirect('configuracao');        
    }
    
}

?>